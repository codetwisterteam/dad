package com.dad.datafeed.data.dao.impl;

import com.dad.datafeed.data.dao.UserDao;
import com.dad.datafeed.model.User;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserDaoImpl extends BaseDataFeedHibernateDaoSupport<User,Long> implements UserDao {
    @Override
    public List<User> getUserList() {
        CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root);
        Query<User> query = getCurrentSession().createQuery(criteria);
        return query.getResultList();
    }
}
