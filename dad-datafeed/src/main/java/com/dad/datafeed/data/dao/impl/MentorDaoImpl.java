package com.dad.datafeed.data.dao.impl;

import com.dad.datafeed.data.dao.MentorDao;
import com.dad.datafeed.model.Mentor;
import org.springframework.stereotype.Repository;

@Repository
public class MentorDaoImpl extends BaseDataFeedHibernateDaoSupport<Mentor,Long> implements MentorDao {
}
