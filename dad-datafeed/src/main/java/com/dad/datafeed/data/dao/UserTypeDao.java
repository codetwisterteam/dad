package com.dad.datafeed.data.dao;

import com.dad.datafeed.model.UserType;

public interface UserTypeDao extends IBaseDataFeedHibernateDao<UserType,Long>{
}
