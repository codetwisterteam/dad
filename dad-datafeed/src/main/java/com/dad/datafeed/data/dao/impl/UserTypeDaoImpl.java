package com.dad.datafeed.data.dao.impl;

import com.dad.datafeed.data.dao.UserTypeDao;
import com.dad.datafeed.model.UserType;
import org.springframework.stereotype.Repository;

@Repository
public class UserTypeDaoImpl extends BaseDataFeedHibernateDaoSupport<UserType,Long> implements UserTypeDao {
}
