package com.dad.datafeed.data.dao;

import com.dad.datafeed.model.Mentor;

public interface MentorDao extends IBaseDataFeedHibernateDao<Mentor,Long> {
}
