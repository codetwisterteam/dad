package com.dad.datafeed.data.dao;

import com.dad.datafeed.model.User;

import java.util.List;

public interface UserDao extends IBaseDataFeedHibernateDao<User,Long> {
    List<User> getUserList();
}

