package com.dad.datafeed.data.service.impl;

import com.dad.datafeed.data.dao.IBaseDataFeedHibernateDao;
import com.dad.datafeed.data.service.IBaseDatafeedService;
import com.dad.datafeed.model.AbstractBaseDataFeedEntity;
import com.dad.exception.DadDatabaseException;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Transactional
public abstract class BaseDatafeedService<T extends AbstractBaseDataFeedEntity, ID extends Serializable> implements IBaseDatafeedService<T, ID> {
    protected IBaseDataFeedHibernateDao<T, ID> getDaoTemplate;

    @Override
    public ID persist(T newInstance) {
        return getDaoTemplate.persist(newInstance);
    }

    @Override
    public void update(T transientObject) {
        getDaoTemplate.update(transientObject);
    }

    @Override
    public void delete(T persistentObject) throws DadDatabaseException {
        getDaoTemplate.delete(persistentObject);
    }

    @Override
    public T get(ID id) {
        return getDaoTemplate.get(id);
    }

    @Override
    public T load(ID id) {
        return getDaoTemplate.load(id);
    }

    @Override
    public List<T> findAll() {
        return getDaoTemplate.findAll();
    }

    @Override
    public ID getIdentifier(T transientObject) {
        return getDaoTemplate.getIdentifier(transientObject);
    }
}
