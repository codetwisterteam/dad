package com.dad.datafeed.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mentor", schema = "dad")
public class Mentor extends AbstractBaseDataFeedEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToMany(mappedBy = "mentor")
    private List<User> users = new ArrayList<>();

    @Column(name = "MENTOR_UID", nullable = false)
    private String mentorUID;

    @Column(name = "COMPANY")
    private String company;

    @Column(name = "ACTIVE")
    private Boolean active;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getMentorUID() {
        return mentorUID;
    }

    public void setMentorUID(String mentorUID) {
        this.mentorUID = mentorUID;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
