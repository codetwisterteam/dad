package com.dad.datafeed.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_type", schema = "dad")
public class UserType extends AbstractBaseDataFeedEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User userID;

    @Column(name = "TYPE", nullable = false)
    private String type;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public User getUserID() {
        return userID;
    }

    public void setUserID(User userID) {
        this.userID = userID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
