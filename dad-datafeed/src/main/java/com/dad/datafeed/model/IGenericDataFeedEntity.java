package com.dad.datafeed.model;

/**
 * @author ferhatyaman<br>
 * AbstractBaseEntity nin implement ettigi ve her entity nin temel degiskenlerini barindiran arayuzdur.
 */
public interface IGenericDataFeedEntity {


    Long getId();

    void setId(Long id);

}

