package com.dad.bo.util;

import com.dad.log.util.DadLogConfiguration;
import com.dad.service.util.DadServiceConfiguration;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("com.dad.bo")
@Import({DadServiceConfiguration.class, DadLogConfiguration.class})
public class DadBoConfiguration {
    public DadBoConfiguration(){System.out.println("DadBoConfiguration is initialized");}

    @Bean
    public static CustomScopeConfigurer customScopeConfigurer(){
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        Map<String,Object> scopes = new HashMap<>();
//        scopes.put("view",new ViewScope());
//        configurer.setScopes(scopes);
        return configurer;
    }
}
