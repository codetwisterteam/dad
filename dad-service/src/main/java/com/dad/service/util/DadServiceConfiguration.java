package com.dad.service.util;

import com.dad.bocontroller.util.DadBoControllerConfiguration;
import com.dad.datafeed.util.DadDataFeedConfiguration;
import com.dad.log.util.DadLogConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.dad.service")
@Import({DadLogConfiguration.class, DadBoControllerConfiguration.class, DadDataFeedConfiguration.class,DadHibernateConfiguration.class})
public class DadServiceConfiguration {
}
