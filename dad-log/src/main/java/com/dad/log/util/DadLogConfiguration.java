package com.dad.log.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dad.log")
public class DadLogConfiguration {
    public DadLogConfiguration(){}
}
