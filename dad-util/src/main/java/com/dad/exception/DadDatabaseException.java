package com.dad.exception;

import java.io.Serializable;
import java.sql.SQLException;

public class DadDatabaseException extends SQLException {
    /**
     *
     */
    public DadDatabaseException() {
        super();
    }

    /**
     * @param message
     */
    public DadDatabaseException(String message) {
        super(message);
    }

    /**
     * @param throwable
     */
    public DadDatabaseException(Throwable throwable) {
        super(throwable);
    }

    /**
     * @param message
     * @param throwable
     */
    public DadDatabaseException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public <ID extends Serializable> DadDatabaseException(String msgKey, ID id, String simpleName) {

    }
}
