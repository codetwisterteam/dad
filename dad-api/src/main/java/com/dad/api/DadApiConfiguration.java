package com.dad.api;

import com.dad.log.util.DadLogConfiguration;
import com.dad.service.util.DadServiceConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.dad.api")
@Import({DadServiceConfiguration.class, DadLogConfiguration.class})
public class DadApiConfiguration {
}
