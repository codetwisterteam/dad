package com.dad.bocontroller.util;

import com.dad.log.util.DadLogConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.dad.bocontroller")
@Import(DadLogConfiguration.class)
public class DadBoControllerConfiguration {
}
